iOS-MultiView
=============

Exercises from the Chapter 6 of the book Beginning iOS6 Development (http://www.apress.com/9781430245124).
This is a simple example meant to show the basics of having several views in an iOS app and how to interact with them.
