//
//  BIDAppDelegate.h
//  ViewSwitcher
//
//  Created by manu on 04/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
