//
//  BIDBlueSwitchViewController.m
//  ViewSwitcher
//
//  Created by manu on 04/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "BIDBlueSwitchViewController.h"

@interface BIDBlueSwitchViewController ()

@end

@implementation BIDBlueSwitchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
